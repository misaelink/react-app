import './App.css';
import sortCategoriesForInsert from './sortCategories';

const unsortedCategories = [
  { name: 'Wallet1', id: 156, parent_id: 56 },
  { name: 'Watches', id: 57, parent_id: 1 },
  { name: 'Men', id: 20, parent_id: null },
  { name: 'Wallets', id: 56, parent_id: 1 },
  { name: 'Cosmetics', id: 58, parent_id: 2 },
  { name: 'Wallet3', id: 158, parent_id: 56 },
  { name: 'Women', id: 21, parent_id: null },
  { name: 'Cosmetic Bio', id: 55, parent_id: 2 },
  { name: 'Cosmetic2', id: 257, parent_id: 58 },
  { name: 'Wallet2', id: 157, parent_id: 56 },
  { name: 'Cosmetic1', id: 256, parent_id: 58 },
  { name: 'Accesories', id: 1, parent_id: 20 },
  { name: 'Accesories', id: 2, parent_id: 21 },
];

function App() {
  const sortedCategories = sortCategoriesForInsert(unsortedCategories);
  const buildNestedCategories = (categories, id = null) => {
    return categories
                .filter(cat => cat['parent_id'] === id)
                .map(cat => ({ ...cat, children: buildNestedCategories(categories, cat.id) }));
  };
  const nestedCategories = buildNestedCategories(sortedCategories);

  const ListItem = ({ item }) => {
    let catChildren = null;
    if (item.children && item.children.length) {
      catChildren = (
        <ul>
          {item.children.map(i => <ListItem item={i} key={i.id} />)}
        </ul>
      );
    }
    return (
      <>
        <li>
          {item.name}
        </li>
        {catChildren}
      </>
    );
  }

  return (
    <div className="App">
      <ul>
        {nestedCategories.map(i => (
          <ListItem item={i} key={i.id} />
        ))}
      </ul>
    </div>
  );
}

export default App;
