module.exports = function sortCategoriesForInsert(inputJson) {
	const mapOfCategories = {};
	let initSortedCategories = [];
	inputJson.forEach((category) => {
		if (category.parent_id === null) {
			initSortedCategories.push(category);
			return;
		};
		if (!mapOfCategories[category.parent_id]) {
			mapOfCategories[category.parent_id] = [];	
		} 
		mapOfCategories[category.parent_id].push(category);
	});
	
	const fillSortedCategories = (sortedCategories, mapOfCategories) => {
		sortedCategories.some((sortedCategory, index) => {
			if (mapOfCategories[sortedCategory.id.toString()]) {
				sortedCategories.splice(index + 1, 0, ...mapOfCategories[sortedCategory.id.toString()]);
				delete mapOfCategories[sortedCategory.id.toString()];
				if(Object.keys(mapOfCategories).length === 0) {
					initSortedCategories = [...sortedCategories];
				}
				return fillSortedCategories(sortedCategories, mapOfCategories);

			}
			return false;
		});
	}
	fillSortedCategories(initSortedCategories, mapOfCategories);
	return initSortedCategories;
}